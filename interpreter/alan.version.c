/* alan.c - Created by venum 4.2 beta */

#include "alan.version.h"

Product alan = {
  "Alan",
  "Adventure Language System",
  "Alan 3.0beta6",
  "Alan 3.0beta6 -- Adventure Language System (2016-09-30 01:05)",
  "2016-09-30",
  "01:05:09",
  "Thomas",
  "thoni64",
  "cygwin32",
  {"3.0beta6", 3, 0, 6, 1475190309, "beta"}
};

static char *alanId =
  "@(#)RELEASE ";
