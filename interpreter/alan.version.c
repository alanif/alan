/* alan.c - Created by venum 4.2 beta */

#include "alan.version.h"

Product alan = {
  "Alan",
  "Adventure Language System",
  "Alan 3.0beta6",
  "Alan 3.0beta6 -- Adventure Language System (2019-05-07 22:53)",
  "2019-05-07",
  "22:53:10",
  "Thomas",
  "thoni64",
  "cygwin64",
  {"3.0beta6", 3, 0, 6, 1557262390, "beta"}
};

char *alanId =
  "@(#)RELEASE ";
