#######################################################################
include ../mk/include_guard.mk
#
# Common rules for alan compiler and variants

ifneq ($(BUILDNUMBER),)
  # For snapshots we use buildnumber for version info and filenames
  BUILDVERSION = -$(BUILDNUMBER)
endif

CFLAGS += -g -Wall -MMD -I../interpreter -DBUILD=$(BUILDNUMBER)

COMPILE = $(CC) $(CFLAGS)
LINK = $(CC) $(LDFLAGS)

gprof: CFLAGS += -pg
gprof: LDFLAGS += -pg


######################################################################
# Target to just build
.PHONY: build gprof
build gprof: alan $(EXTRA_TARGETS)


#######################################################################
#
# Run all tests!
#
UNITOUTPUT ?= -c

.PHONY: test
test: standard_tests $(EXTRA_TESTS)

standard_tests:
	@../bin/jregr -dir testing $(JREGROUTPUT)
	@../bin/jregr -dir testing/positions $(JREGROUTPUT)
	@../bin/jregr -dir testing/dump $(JREGROUTPUT)
	@../bin/jregr -dir testing/arguments $(JREGROUTPUT)
	@../bin/jregr -dir ../regression/platforms $(JREGROUTPUT)
	@# Generate a new .a3c file for this platform that can be committed
	@# so that it can be tested on other platforms
	@cd ../regression/platforms; cp platforms.alan $(OS).alan; ../../bin/alan $(OS); rm $(OS).alan
# TODO: Backwards compatibility tests for the compiler (for new or changed language constructs) are not working
#	@../bin/jregr -bin bin -dir regression/versions/compiler $(JREGROUTPUT)

# Clean
.PHONY: clean
clean:
	-rm -rf $(UNITTESTS_OBJDIR) $(ALANOBJDIR) $(WINALANOBJDIR) *.$(SOEXTENSION) alan{,.exe} winalan{,.exe} ../bin/alan{,.exe} ../bin/winalan{,.exe} alan.res

#######################################################################
#
# Alan command line compiler - native
#
ALANOBJDIR = .alan
ALANOBJECTS = $(addprefix $(ALANOBJDIR)/,${ALANSRCS:.c=.o}) $(ALANOBJDIR)/alan.version.o
-include $(ALANOBJECTS:.o=.d)
$(ALANOBJECTS): $(ALANOBJDIR)/%.o: %.c
	$(COMPILE) -o $@ -c $<

alan: ../bin $(ALANOBJECTS)
	$(LINK) -o $@$(EXEEXT) $(ALANOBJECTS) $(LDLIBS)
	cp $@$(EXEEXT) ../bin

$(ALANOBJECTS): | $(ALANOBJDIR)
$(ALANOBJDIR):
	@mkdir $(ALANOBJDIR)

../bin:
	-mkdir ../bin

#################################################################
#
# Unit testing
#
.PHONY: unit
ifneq ($(CGREEN),yes)
unit:
	@echo "No unit tests run, cgreen not available"
unitbuild:
else
.PHONY: unit
# Two types:
unit: linked_unittests isolated_unittests
.PHONY: unitbuild
unitbuild: linked_unittests_build isolated_unittests_build
endif

UNITTESTS_OBJDIR = .unittests
UNITTESTSOBJECTS = $(addprefix $(UNITTESTS_OBJDIR)/,${UNITTESTSSRCS:.c=.o}) $(UNITTESTS_OBJDIR)/alan.version.o
UNITTESTSDLLOBJECTS = $(addprefix $(UNITTESTS_OBJDIR)/,${UNITTESTSDLLSRCS:.c=.o}) $(UNITTESTS_OBJDIR)/alan.version.o

# Dependencies, if they don't exist yet
-include $(UNITTESTSOBJECTS:.o=.d)

# Rule to compile unittest objects to subdirectory
$(UNITTESTS_OBJDIR)/%.o: %.c
	$(COMPILE) -o $@ -c $<
$(UNITTESTS_OBJDIR)/%_tests.o: %_tests.c
	$(COMPILE) -o $@ -c $<

$(UNITTESTSOBJECTS): | $(UNITTESTS_OBJDIR)

###################################################################
# Build a DLL of all unittests in a separate dir that should exist...
linked_unittests_build: unittests.$(SOEXTENSION)

unittests.$(SOEXTENSION): $(UNITTESTSOBJECTS)
	$(LINK) -shared -o $@ $(UNITTESTSDLLOBJECTS) $(LDLIBS) -lcgreen

# ... that can be run with the cgreen runner
linked_unittests: unittests.$(SOEXTENSION)
	cgreen-runner ./$^ --suite compiler_unit_tests $(UNITOUTPUT)

#####################################################################
# Build isolated unittests in shared libs for each module where it can
# be tested in total isolation (with everything else mocked away,
# except some extra objects)

ISOLATED_UNITTESTS_EXTRA_MODULES = util options sysdep lst dump opt type alan.version

isolated_unittests: SUITE = Compiler

include ../mk/isolated_unittests.mk

######################################################################
#
# Coverage
#
#   make coverage - rebuilds everything for coverage, runs all tests,
#                   generates and opens report
#   make coverage_build - builds what has changed for coverage
#   make coverage_report - generates the report

coverage_build: CFLAGS += --coverage -fPIC
coverage_build: LDFLAGS += --coverage

coverage: clean coverage_build unit test coverage_report
	open coverage/index.html

coverage_build: build unitbuild
	@echo "***********************************"
	@echo "Run your tests and then 'make coverage_report'"

coverage_report:
	lcov --capture --directory . -b . --output-file coverage_tmp.info
	lcov --extract coverage_tmp.info '*.c' -o coverage.info
	genhtml coverage.info --output coverage
