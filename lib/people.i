-- person.i
-- Library version 0.6.0


-- Classes for people

Every person Isa actor
End Every person.

Every male Isa person
  Pronoun him.
End Every male.

Every female Isa person
  Pronoun her.
End Every female.
