/* alan.c - Created by venum 4.2 beta */

#include "alan.version.h"

Product alan = {
  "Alan",
  "Adventure Language System",
  "Alan 3.0beta6",
  "Alan 3.0beta6 -- Adventure Language System (2019-05-27 22:39)",
  "2019-05-27",
  "22:39:09",
  "Thomas",
  "thoni64",
  "cygwin64",
  {"3.0beta6", 3, 0, 6, 1558989549, "beta"}
};

char *alanId =
  "@(#)RELEASE ";
