/* alan.c - Created by venum 4.2 beta */

#include "alan.version.h"

Product alan = {
  "Alan",
  "Adventure Language System",
  "Alan 3.0beta5",
  "Alan 3.0beta5 -- Adventure Language System (2017-02-17 08:59)",
  "2017-02-17",
  "08:59:57",
  "Thomas",
  "thoni64",
  "cygwin32",
  {"3.0beta5", 3, 0, 5, 1487318397, "beta"}
};

static char *alanId =
  "@(#)RELEASE ";
